<?php

return [
    'og_url' => env('OG_URL'),
    'og_title' => env('OG_TITLE'),
    'og_description' => env('OG_DESCRIPTION'),
    'og_image' => env('OG_IMAGE'),
    'og_type' => env('OG_TYPE'),
    'og_site_name' => env('OG_SITE_NAME'),
];