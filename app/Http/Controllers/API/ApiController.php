<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class ApiController extends Controller
{
    public function users() {
        $users = User::all();
        $result = $users;
        
        return response()->json($result);
    }
}
