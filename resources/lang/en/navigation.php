<?php

return [
    'welcome' => 'Welcome',
    'router' => 'Router',
    'about' => 'About',
    'home' => 'Dashboard',
    'account' => 'My account',
    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'email' => 'E-mail',
    'password' => 'Password',
    'remember' => 'Remember me',
    'forgot-your-password' => 'Forgot your password ?'
];