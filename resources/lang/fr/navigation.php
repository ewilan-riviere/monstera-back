<?php

return [
    'welcome' => 'Accueil',
    'router' => 'Routes',
    'about' => 'À propos',
    'home' => 'Tableau de board',
    'account' => 'Mon compte',
    'login' => 'Connexion',
    'logout' => 'Déconnexion',
    'register' => 'S\'enregistrer',
    'email' => 'E-mail',
    'password' => 'Mot de passe',
    'remember' => 'Se rappeler de moi',
    'forgot-your-password' => 'Oubli du mot de passe ?'
];