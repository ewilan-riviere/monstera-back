@extends('layouts.layout')

@section('title')
    {{ __('navigation.home') }}
@endsection

@section('content')
    <div class="title m-b-md">
        {{ __('navigation.home') }}
    </div>
@endsection