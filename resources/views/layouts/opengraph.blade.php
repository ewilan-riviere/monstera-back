<meta property="og:url" content="{{ config('opengraph.og_url') }}">
<meta property="og:title" content="{{ config('opengraph.og_title') }}">
<meta property="og:description" content="{{ config('opengraph.og_description') }}">
<meta property="og:image" content="{{ config('opengraph.og_image') }}">
<meta property="og:type" content="{{ config('opengraph.og_type') }}">
<meta property="og:site_name" content="{{ config('opengraph.og_site_name') }}">